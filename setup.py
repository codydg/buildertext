import setuptools

setuptools.setup(
    name="builder_text",
    version="0.0.1",
    author="Cody Graham",
    author_email="codydgraham@gmail.com",
    description="A helper tool for a specific task",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    setup_requires=['wheel'],
    url='https://gitlab.com/codydg/buildertext'
)
