import cv2
import numpy as np
import pyautogui
from time import sleep
import pytesseract
import re


DEBUG_MODE = False


def take_screenshot(wait_time=None):
    if wait_time is not None:
        sleep(wait_time)
    # make a screenshot
    img = pyautogui.screenshot()
    # convert these pixels to a proper numpy array to work with OpenCV
    im_frame = np.array(img)
    # convert colors from BGR to RGB
    return cv2.cvtColor(im_frame, cv2.COLOR_BGR2RGB)


def get_text_image(image):
    hsv = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)
    in_range = cv2.inRange(hsv, (90, 0, 250), (100, 255, 255))
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
    in_range = cv2.erode(in_range, kernel)
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (15, 15))
    in_range = cv2.dilate(in_range, kernel)
    in_range = cv2.morphologyEx(in_range, cv2.MORPH_CLOSE, kernel=kernel)
    contours = cv2.findContours(in_range, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours = contours[0] if len(contours) == 2 else contours[1]
    contours = sorted(contours, key=cv2.contourArea, reverse=True)
    if len(contours) > 0:
        x, y, w, h = cv2.boundingRect(contours[0])
        image = image[y:y + h, x:x + w]

    image = cv2.bitwise_not(image)
    hsv = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)
    return cv2.bitwise_or(cv2.inRange(hsv, (30, 180, 180), (30, 255, 255)),
                          cv2.inRange(hsv, (11, 180, 180), (11, 255, 255))), \
           cv2.inRange(hsv, (0, 0, 255), (0, 0, 255))


def get_text(image):
    return pytesseract.image_to_string(image, lang='eng', config='--psm 6')


def run():
    mode = 'search'
    data = dict()
    while True:
        if cv2.waitKey(1) == ord('q'):
            break
        frame = take_screenshot()
        needed_text_im, black_text_image = get_text_image(frame)
        if "Required Resources" in get_text(black_text_image):
            if mode == 'search':
                print('Recording')
                mode = 'record'
            needed_text = get_text(needed_text_im)
            if DEBUG_MODE:
                cv2.imshow("resources", needed_text_im)
                print(needed_text)
            results = re.findall(r"(.*)\s*\n-([^\s]*)", needed_text)
            for result in results:
                data[result[0]] = result[1]
        elif mode == 'record':
            print("Recorded data:")
            body = '\n'.join([key + ': ' + data[key] for key in data])
            # send_email('Resources', body)
            print(body)
            print()
            mode = 'search'
            data = dict()
